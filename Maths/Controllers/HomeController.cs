﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MathNet.Numerics.Statistics;
using System.Web.Script.Serialization;

namespace Maths.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult PlotData()
        {
            return View();
        }

        public ActionResult MatchHeight()
        {
            return View();
        }


        public ActionResult TestVKP(int? Buckets)
        {
            List<KeyValuePair<string, string>> columns = new List<KeyValuePair<string, string>>();

            //Generate columns  
            columns.Add(new KeyValuePair<string, string>("string", "Month"));
            columns.Add(new KeyValuePair<string, string>("number", "Bolivia"));


            List<KeyValuePair<string, List<double>>> data = new List<KeyValuePair<string, List<double>>>();
            //Load data  
            data.Add(new KeyValuePair<string, List<double>>("2004/05", new List<double>() { 165}));
            data.Add(new KeyValuePair<string, List<double>>("2004/06", new List<double>() { 135}));
            data.Add(new KeyValuePair<string, List<double>>("2004/07", new List<double>() { 157}));
            data.Add(new KeyValuePair<string, List<double>>("2004/08", new List<double>() { 139}));
            data.Add(new KeyValuePair<string, List<double>>("2004/09", new List<double>() { 136}));

            //Prepare data for JS and return as JSON response  

            string json = new JavaScriptSerializer().Serialize(new
            {
                Options = new { title = "Monthly Coffee Production by Country", vAxisTitle = "Cups", hAxisTitle = "Months" },
                DataColumns = columns,
                DataRows = data
            });


            return Content(json);
        }

        public ActionResult getData(int? id, String name)
        {

            /*
            if (id == null)
            {
                return Json(null);
            }            

            ApplicationDbContext db = new ApplicationDbContext();            

            Unit unit = db.Units.Where(i => i.SerialNumber == id.ToString()).SingleOrDefault();
            if (unit == null)
            {
                return Content("{ERROR_SERIAL_NOT_KNOWN_OR_MULTIPLE_FOUND}{SN" + id + "}");
            }
           
            var unitSensorData = db.SensorData.Where(a => a.Unit.SerialNumber == id.ToString());
            //get data from database, serialise the data to provide as google chart needs
            */
            String mockJson =
                "{" +
                    "\"cols\":[" +
                        "{\"id\":\"\",\"label\":\"X\",\"pattern\":\"\",\"type\":\"number\"}," +
                        "{\"id\":\"\",\"label\":\"" + name + "\",\"pattern\":\"\",\"type\":\"number\"}," +
                        "{\"id\":\"\",\"label\":\"Cats\",\"pattern\":\"\",\"type\":\"number\"}" +
                    "]," +
                    "\"rows\":[" +
                        "{\"c\":[{\"v\":0},{\"v\":3,\"f\":null},{\"v\":3,\"f\":null}]}," +
                        "{\"c\":[{\"v\":1.6,\"f\":null},{\"v\":10,\"f\":null},{\"v\":5,\"f\":null}]}," +
                        "{\"c\":[{\"v\":2,\"f\":null},{\"v\":23,\"f\":null},{\"v\":15,\"f\":null}]}," +
                        "{\"c\":[{\"v\":3,\"f\":null},{\"v\":17,\"f\":null},{\"v\":9,\"f\":null}]}," +
                        "{\"c\":[{\"v\":4,\"f\":null},{\"v\":18,\"f\":null},{\"v\":10,\"f\":null}]}" +
                    "]" +
                "}";


            return Content(mockJson);
            //return Json(unitSensorData.ToList());
        }

        public ActionResult ShowHistogram()
        {
            ViewBag.Message = "Histogram.";

            var myData = new List<double>();
  
            Random random = new Random();
            for (int i = 0; i < 600; i++)
            {
                myData.Add(random.Next(1490580605, 1490584605));
            }

            for (int i = 0; i < 200; i++)
            {
                myData.Add(random.Next(1490584405, 1490584605));
            }


            int numberBuckets = 4;
            var myHistogram = new Histogram(myData, numberBuckets);
          
            String ret = "";
            

            for (int i = 0; i < myHistogram.BucketCount; i++)
            {
                ret = ret + "Bucket " + i + " Count: " + myHistogram[i].Count.ToString() + "<br /> " + (int)myHistogram[i].Width + "<br />" ;
            }
            
           
            
            
            return Content(ret + "<br />"+ myHistogram.ToString() + "<br />" + myData.ToString());
        }


        public ActionResult GetMathData(int Buckets = 4) 
        {

            var myData = new List<double>();

            Random random = new Random();
            for (int i = 0; i < 600; i++)
            {
                myData.Add(random.Next(1490580605, 1490584605));
            }

            for (int i = 0; i < 100; i++)
            {
                myData.Add(random.Next(1490582405, 1490583605));
            }
  
            var myHistogram = new Histogram(myData, Buckets);

            String ret = "";


            for (int i = 0; i < myHistogram.BucketCount; i++)
            {
                ret = ret + "Bucket " + i + " Count: " + myHistogram[i].Count.ToString() + "<br /> " + (int)myHistogram[i].Width + "<br />";
            }
        

          //  return Content(ret + "<br />" + myHistogram.ToString() + "<br />" + myData.ToString());

            List<KeyValuePair<string, string>> columns = new List<KeyValuePair<string, string>>();

            //Generate columns  
            columns.Add(new KeyValuePair<string, string>("string", "Bucket"));
            columns.Add(new KeyValuePair<string, string>("number", "Contacts"));


            List<KeyValuePair<string, List<double>>> data = new List<KeyValuePair<string, List<double>>>();
            //Load data  

            for (int i = 0; i < myHistogram.BucketCount; i++)
            {
                data.Add(new KeyValuePair<string, List<double>>("Bucket " + i, new List<double>() { myHistogram[i].Count }));
             
            }

            //Prepare data for JS and return as JSON response  

            string json = new JavaScriptSerializer().Serialize(new
            {
                Options = new { title = "Monthly Coffee Production by Country", vAxisTitle = "Cups", hAxisTitle = "Months" },
                DataColumns = columns,
                DataRows = data
            });


            return Content(json);
        }
    }

}
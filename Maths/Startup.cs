﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Maths.Startup))]
namespace Maths
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
